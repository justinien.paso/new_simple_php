<?php
use PHPUnit\Framework\TestCase;

class CalculatorTest extends TestCase {
    public function testAdd() {
        $calculator = new Calculator;
        $result = $calculator->add(20, 5);
        $result = $calculator->add(5, 20);

        $this->assertEquals(25, $result);
    }

    public function testSubtract() {
        $calculator = new Calculator;
        $result = $calculator->subtract(20, 5);

        $this->assertEquals(15, $result);
    }

    public function testMultiply() {
        $calculator = new Calculator;
        $result = $calculator->multiply(5, 5);

        $this->assertEquals(25, $result);
    }
}