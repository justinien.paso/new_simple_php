<?php

if ($argc != 3) {
    echo "Usage: " . $argv[0] . " <path/to/index.xml> <threshold>
";
    exit(-1);
}

$file = $argv[1];
$threshold = (double) $argv[2];

$coverage = simplexml_load_file($file);
$branchCoverage = simplexml_load_file("build/coverage.cobertura.xml");

$ratio = (double) $coverage->project->directory->totals->lines["percent"];
$branchRatio = round($branchCoverage->packages->package["branch-rate"] * 100, 2);

echo "Line coverage: $ratio%
";
echo "Branch coverage: $branchRatio%
";
echo "Threshold: $threshold%
";

if ($ratio < $threshold || $branchRatio < $threshold) {
    echo "FAILED!
";
    exit(-1);
}

echo "SUCCESS!
";